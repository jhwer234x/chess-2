import general.Piece;

import javax.imageio.ImageIO;
import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Hashtable;

public class ChessView implements ActionListener {
    private final static int SCREEN_WIDTH = 700;
    private final static int SCREEN_HEIGHT = 700;
    final private JButton[][] buttonGrid;
    final private JFrame window;
    final private Hashtable<String, JMenuItem> specialButtons;

    /**
     * Constructor of a Chess view
     *
     * @param row    the width of a chess board
     * @param column the height of a chess board
     */
    public ChessView(int row, int column) {
        try {
            UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
        } catch (Exception e) {
            //silently ignore
        }
        buttonGrid = new JButton[row][column];
        specialButtons = new Hashtable<>();
        window = new JFrame("WHITE");
        window.setSize(SCREEN_WIDTH, SCREEN_HEIGHT);
        JPanel myPanel = initializePanel();
        initializeButton(myPanel);
        setUpMenu(window);
        window.setContentPane(myPanel);
        window.setVisible(true);
        window.setResizable(false);
        window.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    }

    public void setTitle(String title) {
        this.window.setTitle(title);
    }

    public JButton[][] getButtonGrid() {
        return buttonGrid;
    }

    public Hashtable<String, JMenuItem> getSpecialButtons() {
        return specialButtons;
    }


    /**
     * Create a alert box to notify user of an important status update
     *
     * @param line the message containing important status
     */
    public void alertBox(String line) {
        JOptionPane.showMessageDialog(null,
                line,
                "Notification", JOptionPane.INFORMATION_MESSAGE);
    }

    /**
     * Create an alert box and get input from a user
     *
     * @param line the message containing important status
     * @return the string user input to the controller
     */
    public String showInputBox(String line) {
        return JOptionPane.showInputDialog(null,
                line,
                "Input", JOptionPane.INFORMATION_MESSAGE);
    }


    /**
     * Given list of all pieces place them on the board
     *
     * @param pieces that to be placed on the board
     * @throws IOException
     */
    public void showPieces(ArrayList<Piece> pieces) throws IOException {
        clearBoard();

        Image img;
        //Setup Pieces
        for (Piece p : pieces) {
            Piece.PieceColor color = p.getColor();
            String imagePath = "resources/" + getPieceName(p);
            if (color == Piece.PieceColor.WHITE) {
                imagePath += "White";
            } else {
                imagePath += "Black";
            }
            imagePath += ".png";
            img = ImageIO.read(getClass().getResource(imagePath));
            setPieceImage(p.getCoordinate().getRow(), p.getCoordinate().getColumn(), img);
        }
    }

    @Override
    public void actionPerformed(ActionEvent e) {

    }

    /**
     * Helper function to the constructor. Given a view size create, right number of chess buttons in a GridLayout
     *
     * @return JPanel that is set up according to the board model
     */
    private JPanel initializePanel() {
        int numOfRows = buttonGrid.length;
        int numOfColumns = buttonGrid[0].length;

        JPanel myPanel = new JPanel();
        myPanel.setPreferredSize(new Dimension(SCREEN_WIDTH, SCREEN_HEIGHT));
        myPanel.setLayout(new GridLayout(numOfRows, numOfColumns));
        return myPanel;
    }

    /**
     * Helper function to the constructor. Given a panel, set up appropriate buttons
     *
     * @param myPanel the Jpanel to set up the buttons
     */
    private void initializeButton(JPanel myPanel) {
        int numOfRows = buttonGrid.length;
        int numOfColumns = buttonGrid[0].length;

        for (int row = 0; row < numOfRows; row++) {
            for (int col = 0; col < numOfColumns; col++) {
                //Create checker pattern
                buttonGrid[row][col] = new JButton();
                buttonGrid[row][col].setOpaque(true);
                buttonGrid[row][col].setBorder(null);
                myPanel.add(buttonGrid[row][col]);
            }
        }
        checkerStyle();
    }

    /**
     * For the GUI, set up option menu with special buttons
     *
     * @param window the window to add menu to
     */
    private void setUpMenu(JFrame window) {
        JMenuBar menubar = new JMenuBar();
        JMenu specials = new JMenu("Specials");

        setupMenuHelper(specials, "Undo");
        setupMenuHelper(specials, "Score");
        setupMenuHelper(specials, "Forfeit");
        setupMenuHelper(specials, "Tie");
        setupMenuHelper(specials, "Reset");

        menubar.add(specials);
        window.setJMenuBar(menubar);
    }

    /**
     * Given a special button type, add it to the JMenu
     * @param specials the JMenu where the new buttons are to be added
     * @param title the name of special button
     */
    private void setupMenuHelper(JMenu specials, String title) {
        JMenuItem menuItem = new JMenuItem(title);
        specialButtons.put(title, menuItem);
        menuItem.addActionListener(this);
        specials.add(menuItem);
    }

    /**
     * For the current view, set up black and white style
     */
    private void checkerStyle() {
        int numOfRows = buttonGrid.length;
        int numOfColumns = buttonGrid[0].length;

        for (int row = 0; row < numOfRows; row++) {
            for (int col = 0; col < numOfColumns; col++) {
                //Create checker pattern
                if (((row) + (col)) % 2 == 0) {
                    buttonGrid[row][col].setBackground(Color.white);
                } else {
                    buttonGrid[row][col].setBackground(Color.black);
                }
                buttonGrid[row][col].setOpaque(true);
                buttonGrid[row][col].setBorder(null);
            }
        }
    }

    /**
     * Given a view, reset to a clean board
     */
    private void clearBoard() throws IOException {
        checkerStyle();
        String imagePath = "resources/" + "blank.png";
        Image img = ImageIO.read(getClass().getResource(imagePath));
        for (int row = 0; row < getButtonGrid().length; row++) {
            for (int col = 0; col < getButtonGrid()[0].length; col++) {
                getButtonGrid()[row][col].setIcon(new ImageIcon(img));
                getButtonGrid()[row][col].setBorder(null);
            }
        }
    }

    /**
     * Helper function for showPieces, Given a coordinate, set up piece image
     *
     * @param row the row to set image to
     * @param col the column to set image to
     * @param img an Image icon of a piece
     */
    private void setPieceImage(int row, int col, Image img) {
        getButtonGrid()[row][col].setIcon(new ImageIcon(img));
        getButtonGrid()[row][col].setBorder(null);
    }

    /**
     * Helper function for showPieces to parse piece name
     *
     * @param piece the piece to look up name for
     * @return the current piece type
     */
    private String getPieceName(Piece piece) {
        String piecePath = piece.getClass().toString().toLowerCase();
        int periodPos = piecePath.lastIndexOf(".");
        piecePath = piecePath.substring(periodPos + 1, piecePath.length());
        return piecePath;
    }
}