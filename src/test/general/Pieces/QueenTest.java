package general.Pieces;

import general.Board;
import general.Coordinate;
import general.History;
import general.Piece;
import org.junit.Test;

import java.util.ArrayList;

import static org.junit.Assert.assertEquals;

public class QueenTest {

    /**
     * Test possible moves of a queen in the original starting board
     *
     * @throws Exception
     */
    @Test
    public void testPossibleMoves() throws Exception {
        Board board = new Board();
        board.setPiecesOnRectangleBoard();
        Piece queen = board.getPiece(new Coordinate(7, 3));
        ArrayList<Coordinate> al = queen.possibleMoves(board);
        assertEquals(0, al.size());
    }

    /**
     * Test possible moves of a queen in the middle of a original starting board
     *
     * @throws Exception
     */
    @Test
    public void testPossibleQueenMovesCenterBoard() throws Exception {
        Board board = new Board();
        board.setPiecesOnRectangleBoard();
        Piece queen = board.getPiece(new Coordinate(7, 3));

        //Move the bishop in the middle
        queen.setCoordinate(new Coordinate(4, 3));

        ArrayList<Coordinate> al = queen.possibleMoves(board);
        assertEquals(19, al.size());
    }

    /**
     * Test possibles moves of a queen in original position in an empty board
     *
     * @throws Exception
     */
    @Test
    public void testPossibleQueenMovesEmptyBoard() throws Exception {
        Board board = new Board();
        History history = new History();
        board.setPiecesOnRectangleBoard();
        Piece queen = board.getPiece(new Coordinate(7, 3));

        //Remove all pieces except the queen
        for (int row = 0; row < board.getHorizontalSize(); row++) {
            for (int col = 0; col < board.getVerticalSize(); col++) {
                if (!(row == 7 && col == 3)) {
                    board.removePiece(new Coordinate(row, col));
                }
            }
        }

        ArrayList<Coordinate> al = queen.possibleMoves(board);
        assertEquals(21, al.size());
    }
}