package general.Pieces;

import general.Board;
import general.Coordinate;
import general.Piece;
import org.junit.Test;

import java.util.ArrayList;

import static org.junit.Assert.assertEquals;

/**
 * Test possible moves of a princess in the original starting board
 */
public class PrincessTest {

    /**
     * Test possible moves of a princess in place of a bishop on the original starting board
     *
     * @throws Exception
     */
    @Test
    public void testPossiblePrincessMoves() throws Exception {
        Coordinate bishopPosition = new Coordinate(7, 2);

        //Set up a board with a special piece
        Board board = new Board();
        board.setPiecesOnRectangleBoard();
        board.removePiece(bishopPosition);
        ArrayList<Piece> allPieces = board.getAllPiece();
        allPieces.add(new Princess(Piece.PieceColor.WHITE, bishopPosition));

        //Test for possible moves
        Piece princess = board.getPiece(bishopPosition);
        ArrayList<Coordinate> al = princess.possibleMoves(board);
        assertEquals(2, al.size());
    }

    /**
     * Test possible moves of a princess positioned in the center on original board
     *
     * @throws Exception
     */
    @Test
    public void testPossiblePrincessMovesCenterBoard() throws Exception {
        Coordinate bishopPosition = new Coordinate(7, 2);

        //Set up a board with a special piece
        Board board = new Board();
        board.setPiecesOnRectangleBoard();
        board.removePiece(bishopPosition);
        ArrayList<Piece> allPieces = board.getAllPiece();
        allPieces.add(new Princess(Piece.PieceColor.WHITE, bishopPosition));

        //Test for possible moves
        Piece princess = board.getPiece(bishopPosition);
        princess.setCoordinate(new Coordinate(3, 3));
        ArrayList<Coordinate> al = princess.possibleMoves(board);
        assertEquals(16, al.size());
    }

    /**
     * Test possible moves of a princess positioned in the bishop position on an empty board
     *
     * @throws Exception
     */
    @Test
    public void testPossiblePrincessMovesEmptyBoard() throws Exception {
        Coordinate bishopPosition = new Coordinate(7, 2);

        //Set up a board with a special piece
        Board board = new Board();
        board.setPiecesOnRectangleBoard();
        board.removePiece(bishopPosition);
        ArrayList<Piece> allPieces = board.getAllPiece();
        allPieces.add(new Princess(Piece.PieceColor.WHITE, bishopPosition));

        //Remove all pieces except the princess
        for (int row = 0; row < board.getHorizontalSize(); row++) {
            for (int col = 0; col < board.getVerticalSize(); col++) {
                if (!(row == bishopPosition.getRow() && col == bishopPosition.getColumn())) {
                    board.removePiece(new Coordinate(row, col));
                }
            }
        }

        //Test for possible moves
        Piece princess = board.getPiece(bishopPosition);
        ArrayList<Coordinate> al = princess.possibleMoves(board);
        assertEquals(11, al.size());
    }
}