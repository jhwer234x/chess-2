package general.Pieces;

import general.Board;
import general.Coordinate;
import general.History;
import general.Piece;
import org.junit.Test;

import java.util.ArrayList;

import static org.junit.Assert.assertEquals;

public class KingTest {

    /**
     * Test possible moves of a king in the original starting board
     *
     * @throws Exception
     */
    @Test
    public void testPossibleMoves() throws Exception {
        Board board = new Board();
        board.setPiecesOnRectangleBoard();
        Piece king = board.getPiece(new Coordinate(7, 4));
        ArrayList<Coordinate> al = king.possibleMoves(board);
        assertEquals(0, al.size());
    }

    /**
     * Test possible moves of a king in the middle of a original starting board
     *
     * @throws Exception
     */
    @Test
    public void testPossibleKingMovesCenterBoard() throws Exception {
        Board board = new Board();
        board.setPiecesOnRectangleBoard();
        Piece king = board.getPiece(new Coordinate(7, 4));

        //Move the king in the middle
        king.setCoordinate(new Coordinate(3, 3));

        ArrayList<Coordinate> al = king.possibleMoves(board);
        assertEquals(8, al.size());
    }

    /**
     * Test possible moves of a king in the middle of an empty board
     *
     * @throws Exception
     */
    @Test
    public void testPossibleKingMovesEmptyBoard() throws Exception {
        Board board = new Board();
        History history = new History();
        board.setPiecesOnRectangleBoard();
        Piece king = board.getPiece(new Coordinate(7, 4));

        //Remove all pieces except the king
        for (int row = 0; row < board.getHorizontalSize(); row++) {
            for (int col = 0; col < board.getVerticalSize(); col++) {
                if (!(row == 7 && col == 4)) {
                    board.removePiece(new Coordinate(row, col));
                }
            }
        }

        ArrayList<Coordinate> al = king.possibleMoves(board);
        assertEquals(5, al.size());
    }
}