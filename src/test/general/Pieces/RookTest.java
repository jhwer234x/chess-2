package general.Pieces;

import general.Board;
import general.Coordinate;
import general.History;
import general.Piece;
import org.junit.Test;

import java.util.ArrayList;

import static org.junit.Assert.assertEquals;

public class RookTest {

    /**
     * Test possible moves of a bishop in the original starting board
     *
     * @throws Exception
     */
    @Test
    public void testPossibleMoves() throws Exception {
        Board board = new Board();
        board.setPiecesOnRectangleBoard();
        Piece bishop = board.getPiece(new Coordinate(7, 0));
        ArrayList<Coordinate> al = bishop.possibleMoves(board);
        assertEquals(0, al.size());
    }

    /**
     * Checking possible rook positions after removing a pawn above
     * rook on bottom right
     *
     * @throws Exception
     */
    @Test
    public void testPossibleVerticalRookMoves() throws Exception {
        Board board = new Board();
        History history = new History();
        //Set up the board
        board.setPiecesOnRectangleBoard();
        Piece rook = board.getPiece(new Coordinate(7, 7));
        board.removePiece(new Coordinate(6, 7));
        ArrayList<Coordinate> al = rook.possibleMoves(board);
        assertEquals(6, al.size());
    }

    /**
     * On a empty board, with a rook located on bottom right
     *
     * @throws Exception
     */
    @Test
    public void testPossibleVerticalAndHorizontalRookMoves() throws Exception {
        Board board = new Board();
        History history = new History();
        board.setPiecesOnRectangleBoard();
        Piece rook = board.getPiece(new Coordinate(7, 7));

        //Remove all pieces except the rook
        for (int row = 0; row < board.getHorizontalSize(); row++) {
            for (int col = 0; col < board.getVerticalSize(); col++) {
                if (!(row == 7 && col == 7)) {
                    board.removePiece(new Coordinate(row, col));
                }
            }
        }

        ArrayList<Coordinate> al = rook.possibleMoves(board);
        assertEquals(14, al.size());
    }
}