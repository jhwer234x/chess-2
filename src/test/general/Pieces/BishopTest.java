package general.Pieces;

import general.Board;
import general.Coordinate;
import general.Piece;
import org.junit.Test;

import java.util.ArrayList;

import static org.junit.Assert.assertEquals;

public class BishopTest {

    /**
     * Test possible moves of a bishop in the original starting board
     *
     * @throws Exception
     */
    @Test
    public void testPossibleBishopMoves() throws Exception {
        Board board = new Board();
        board.setPiecesOnRectangleBoard();
        Piece bishop = board.getPiece(new Coordinate(7, 2));
        ArrayList<Coordinate> al = bishop.possibleMoves(board);
        assertEquals(0, al.size());
    }

    /**
     * Test possible moves of a bishop in the middle of a original starting board
     *
     * @throws Exception
     */
    @Test
    public void testPossibleBishopMovesCenterBoard() throws Exception {
        Board board = new Board();
        board.setPiecesOnRectangleBoard();
        Piece bishop = board.getPiece(new Coordinate(7, 2));

        //Move the bishop in the middle
        bishop.setCoordinate(new Coordinate(3, 3));

        ArrayList<Coordinate> al = bishop.possibleMoves(board);
        assertEquals(8, al.size());
    }

    /**
     * Test possible moves of a bishop in the middle of an empty board
     *
     * @throws Exception
     */
    @Test
    public void testPossibleBishopMovesEmptyBoard() throws Exception {
        Board board = new Board();
        board.setPiecesOnRectangleBoard();
        Piece bishop = board.getPiece(new Coordinate(7, 2));

        //Remove all pieces except the bishop
        for (int row = 0; row < board.getHorizontalSize(); row++) {
            for (int col = 0; col < board.getVerticalSize(); col++) {
                if (!(row == 7 && col == 2)) {
                    board.removePiece(new Coordinate(row, col));
                }
            }
        }
        //Move the bishop in the middle
        bishop.setCoordinate(new Coordinate(3, 3));

        ArrayList<Coordinate> al = bishop.possibleMoves(board);
        assertEquals(13, al.size());
    }
}