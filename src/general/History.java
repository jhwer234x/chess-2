package general;

import general.Pieces.*;

import java.util.ArrayList;

public class History {
    private final ArrayList<ArrayList<Piece>> history = new ArrayList<>();

    public History() {
    }

    /**
     * Remove all history
     */
    public void clear() {
        history.clear();
    }

    /**
     * Remove the last history of ArrayList containing pieces position
     */
    public void removeHistory() {
        history.remove(history.size() - 1);
    }

    /**
     * @return the number of moves taken
     */
    public int numOfMoves() {
        return history.size();
    }

    /**
     * @return the last pieces location
     */
    public ArrayList<Piece> getHistory() {
        return history.remove(history.size() - 1);
    }

    /**
     * Add a list of pieces location to history
     *
     * @param history a list to be added to history
     */
    public void addHistory(ArrayList<Piece> history) {
        ArrayList<Piece> temp = new ArrayList<>();
        for (Piece p : history) {
            if (p instanceof Pawn) {
                temp.add(new Pawn(p.getColor(), new Coordinate(p.coordinate.getRow(), p.coordinate.getColumn())));
            } else if (p instanceof King) {
                temp.add(new King(p.getColor(), new Coordinate(p.coordinate.getRow(), p.coordinate.getColumn())));
            } else if (p instanceof Knight) {
                temp.add(new Knight(p.getColor(), new Coordinate(p.coordinate.getRow(), p.coordinate.getColumn())));
            } else if (p instanceof Queen) {
                temp.add(new Queen(p.getColor(), new Coordinate(p.coordinate.getRow(), p.coordinate.getColumn())));
            } else if (p instanceof Rook) {
                temp.add(new Rook(p.getColor(), new Coordinate(p.coordinate.getRow(), p.coordinate.getColumn())));
            } else if (p instanceof Bishop) {
                temp.add(new Bishop(p.getColor(), new Coordinate(p.coordinate.getRow(), p.coordinate.getColumn())));
            }
        }
        this.history.add(temp);
    }
}
