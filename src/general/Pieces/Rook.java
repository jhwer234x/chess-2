package general.Pieces;

import general.Board;
import general.Coordinate;
import general.Piece;

import java.util.ArrayList;

public class Rook extends Piece {

    public Rook(Piece.PieceColor color, Coordinate coordinate) {
        super();
        this.color = color;
        this.coordinate = coordinate;
    }

    /**
     * Given a piece return appropriate moves
     *
     * @param board the board to move pieces around
     * @return the valid arraylist of coordinates
     */
    @Override
    public ArrayList<Coordinate> possibleMoves(Board board) {
        int cRow = this.coordinate.getRow();
        int cCol = this.coordinate.getColumn();
        ArrayList<Coordinate> possiblePositions = new ArrayList<>();

        //Checking the block if it's onBoard and if there is a different color piece
        //Up
        int rowToCheck = cRow;
        while (rowToCheck >= 0) {
            rowToCheck--;
            if (movement(board, cCol, possiblePositions, rowToCheck)) break;
        }
        //Down
        rowToCheck = cRow;
        while (rowToCheck < board.getVerticalSize()) {
            rowToCheck++;
            if (movement(board, cCol, possiblePositions, rowToCheck)) break;
        }
        //Right
        int colToCheck = cCol;
        while (colToCheck < board.getHorizontalSize()) {
            colToCheck++;
            if (movement(board, colToCheck, possiblePositions, cRow)) break;
        }
        //Left
        colToCheck = cCol;
        while (colToCheck >= 0) {
            colToCheck--;
            if (movement(board, colToCheck, possiblePositions, cRow)) break;
        }
        return possiblePositions;
    }

    /**
     * Helper function for possibleMoves
     *
     * @param board             the board to move a piece
     * @param possiblePositions the arraylist to add new possible positions to
     * @param rowToCheck        the destination row to check
     * @return true if it is a possible move, false otherwise
     */
    private boolean movement(Board board, int cCol, ArrayList<Coordinate> possiblePositions, int rowToCheck) {
        Coordinate destToCheck = new Coordinate(rowToCheck, cCol);
        //If the destination is out of board or has same color
        if (isMoveToSameColor(board, destToCheck) || !board.isOnBoard(destToCheck)) {
            return true;
        } else {
            //If the destination has enemy color, last one to be added
            if (isMoveToEnemyColor(board, destToCheck)) {
                possiblePositions.add(destToCheck);
                return true;
            } else {
                possiblePositions.add(destToCheck);
            }
        }
        return false;
    }
}
