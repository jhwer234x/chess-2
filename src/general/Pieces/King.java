package general.Pieces;

import general.Board;
import general.Coordinate;
import general.Piece;

import java.util.ArrayList;

public class King extends Piece {

    public King(Piece.PieceColor color, Coordinate coordinate) {
        super();
        this.color = color;
        this.coordinate = coordinate;
    }

    /**
     * Given a piece return appropriate moves
     *
     * @param board the board to move pieces around
     * @return the valid arraylist of coordinates
     */
    @Override
    public ArrayList<Coordinate> possibleMoves(Board board) {
        int cRow = this.coordinate.getRow();
        int cCol = this.coordinate.getColumn();
        ArrayList<Coordinate> possiblePositions = new ArrayList<>();

        //Checking the block if it's onBoard and if there is a different color piece

        //Horizontal, Vertical moves
        //Right
        if (kingPossibleMovesHelper(board, new Coordinate(cRow, cCol + 1))) {
            possiblePositions.add(new Coordinate(cRow, cCol + 1));
        }
        //Left
        if (kingPossibleMovesHelper(board, new Coordinate(cRow, cCol - 1))) {
            possiblePositions.add(new Coordinate(cRow, cCol - 1));
        }
        //Up
        if (kingPossibleMovesHelper(board, new Coordinate(cRow - 1, cCol))) {
            possiblePositions.add(new Coordinate(cRow - 1, cCol));
        }
        //Down
        if (kingPossibleMovesHelper(board, new Coordinate(cRow + 1, cCol))) {
            possiblePositions.add(new Coordinate(cRow + 1, cCol));
        }

        //Diagonal moves
        //up-right
        if (kingPossibleMovesHelper(board, new Coordinate(cRow - 1, cCol + 1))) {
            possiblePositions.add(new Coordinate(cRow - 1, cCol + 1));
        }
        //up-left
        if (kingPossibleMovesHelper(board, new Coordinate(cRow - 1, cCol - 1))) {
            possiblePositions.add(new Coordinate(cRow - 1, cCol - 1));
        }
        //down-right
        if (kingPossibleMovesHelper(board, new Coordinate(cRow + 1, cCol + 1))) {
            possiblePositions.add(new Coordinate(cRow + 1, cCol + 1));
        }
        //down-left
        if (kingPossibleMovesHelper(board, new Coordinate(cRow + 1, cCol - 1))) {
            possiblePositions.add(new Coordinate(cRow + 1, cCol - 1));
        }
        return possiblePositions;
    }

    /**
     * Helper function to check going to either empty or enemy coordinate
     *
     * @param board          the board to move the pieces around
     * @param destCoordinate the destination coordinate to move the king
     * @return the possible safe king moves
     */
    private boolean kingPossibleMovesHelper(Board board, Coordinate destCoordinate) {
        return canMoveToEmpty(board, destCoordinate) || isMoveToEnemyColor(board, destCoordinate);
    }

}
