package general.Pieces;

import general.Board;
import general.Coordinate;
import general.Piece;

import java.util.ArrayList;

/**
 * A new type of piece behaving as a knight and bishop
 */
public class Princess extends Piece {

    public Princess(Piece.PieceColor color, Coordinate coordinate) {
        super();
        this.color = color;
        this.coordinate = coordinate;
    }

    /**
     * Given a piece return appropriate moves
     *
     * @param board the board to move pieces around
     * @return the valid arraylist of coordinates
     */
    @Override
    public ArrayList<Coordinate> possibleMoves(Board board) {
        ArrayList<Coordinate> possiblePositions = new ArrayList<>();

        possiblePositions.addAll(new Knight(this.getColor(), this.getCoordinate()).possibleMoves(board));
        possiblePositions.addAll(new Bishop(this.getColor(), this.getCoordinate()).possibleMoves(board));

        return possiblePositions;
    }
}
