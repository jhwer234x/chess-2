package general.Pieces;

import general.Board;
import general.Coordinate;
import general.Piece;

import java.util.ArrayList;

/**
 * A new piece behaving as knight and rook
 */
public class Empress extends Piece {

    public Empress(Piece.PieceColor color, Coordinate coordinate) {
        super();
        this.color = PieceColor.WHITE;
        this.coordinate = coordinate;
    }

    /**
     * Given a piece return appropriate moves
     *
     * @param board the board to move pieces around
     * @return the valid arraylist of coordinates
     */
    @Override
    public ArrayList<Coordinate> possibleMoves(Board board) {
        ArrayList<Coordinate> possiblePositions = new ArrayList<>();

        possiblePositions.addAll(new Knight(this.getColor(), this.getCoordinate()).possibleMoves(board));
        possiblePositions.addAll(new Rook(this.getColor(), this.getCoordinate()).possibleMoves(board));

        return possiblePositions;
    }
}
