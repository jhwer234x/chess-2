package general;

import java.util.ArrayList;

public abstract class Piece {
    protected PieceColor color;
    protected Coordinate coordinate;

    protected Piece() {
        color = null;
        coordinate = null;
    }

    /**
     * @return the color of a chess piece for this Piece object
     */
    public PieceColor getColor() {
        return color;
    }

    /**
     * @return the coordinate number representing the position for this Piece object
     */
    public Coordinate getCoordinate() {
        return coordinate;
    }

    /**
     * Set the coordinate number of a Piece object
     *
     * @param coordinate the row position to set a piece to
     */
    public void setCoordinate(Coordinate coordinate) {
        this.coordinate = coordinate;
    }

    /**
     * Move a piece object on a given board and add new board state to a given history
     *
     * @param board          the board on which to move pieces
     * @param history        the history to save the new board state
     * @param destCoordinate the destination to which to move piece
     * @return true if the move was successful, false otherwise
     */
    public boolean makeMove(Board board, History history, Coordinate destCoordinate) {
        int destinationRow = destCoordinate.getRow();
        int destinationColumn = destCoordinate.getColumn();

        //Check if original position and destination position are both valid
        if (board.isOnBoard(this.coordinate) && board.isOnBoard(destCoordinate)) {
            Piece piece = board.getPiece(this.coordinate);
            if (piece == null) {
                return false;
            }
            ArrayList<Coordinate> possibleMoves = possibleMoves(board);
            for (Coordinate c : possibleMoves) {
                if (c.getRow() == destinationRow && c.getColumn() == destinationColumn) {
                    history.addHistory(board.getAllPiece());
                    //Check if an enemy piece has to be removed
                    if (isMoveToEnemyColor(board, destCoordinate)) {
                        board.removePiece(destCoordinate);
                    }
                    changePiecePosition(board, this, destCoordinate);
                    //If after changing a piece, king same color as the piece is put in check, revert.
                    if (board.isInCheck() != null && board.getPiece(board.isInCheck()).getColor() == color) {
                        board.setPieces(history.getHistory());
                        return false;
                    } else {
                        return true;
                    }
                }
            }
        }
        return false;
    }

    public abstract ArrayList<Coordinate> possibleMoves(Board board);

    /**
     * Helper function for possibleMoves to check if a new destination is on the board
     * and new destination has an empty.
     *
     * @param board      the board we are moving pieces over
     * @param coordinate the destination to perform check
     * @return true is piece can move to the destination, false if not
     */
    protected boolean canMoveToEmpty(Board board, Coordinate coordinate) {
        return board.isOnBoard(coordinate) && board.getPiece(coordinate) == null;
    }

    /**
     * Helper function for possibleMoves to check if a new destination is on the board
     * and new destination has an same color piece
     *
     * @param board      the board we are moving pieces over
     * @param coordinate the destination to perform check
     * @return true is piece can move to the destination, false if not
     */
    protected boolean isMoveToSameColor(Board board, Coordinate coordinate) {
        Piece.PieceColor color = this.getColor();
        Piece p = board.getPiece(coordinate);
        return p != null && p.getColor() == color && board.isOnBoard(coordinate);
    }

    private void changePiecePosition(Board board, Piece piece, Coordinate destCoordinate) {
        for (Piece p : board.getAllPiece()) {
            if (p.getCoordinate() == piece.getCoordinate()) {
                p.setCoordinate(destCoordinate);
            }
        }
    }

    /**
     * Helper function for possibleMoves to check if a new destination is on the board
     * and new destination has an enemy color or empty.
     * Piece can only move when the destination has enemy color
     *
     * @param board      the board we are moving pieces over
     * @param coordinate the destination to perform check
     * @return true is piece can move to the destination, false if not
     */
    protected boolean isMoveToEnemyColor(Board board, Coordinate coordinate) {
        Piece.PieceColor color = this.getColor();
        Piece.PieceColor enemyColor = color.oppositeColor();
        Piece p = board.getPiece(coordinate);
        return p != null && p.getColor() == enemyColor && board.isOnBoard(coordinate);
    }

    public enum PieceColor {
        WHITE,
        BLACK;

        /**
         * Given a piece return opposite color
         *
         * @return white if a piece is black, otherwise return black
         */
        public PieceColor oppositeColor() {
            switch (this) {
                case WHITE:
                    return BLACK;
                case BLACK:
                    return WHITE;
                default:
                    throw new IllegalStateException("This should never happen: " + this + " has no opposite.");
            }
        }
    }
}
