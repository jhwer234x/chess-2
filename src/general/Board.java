package general;

import general.Pieces.*;

import java.util.ArrayList;

/**
 * A Board is an object that holds information about Chess
 */
public class Board {
    private final static int DEFAULT_HEIGHT = 8;
    private final static int DEFAULT_WIDTH = 8;
    private int horizontalSize;
    private int verticalSize;
    private Piece[][] board;
    private ArrayList<Piece> pieces = new ArrayList<>();

    /**
     * Construct a square 8x8 chess board
     */
    public Board() {
        this.verticalSize = DEFAULT_HEIGHT;
        this.horizontalSize = DEFAULT_WIDTH;
        board = new Piece[horizontalSize][verticalSize];
    }

    /**
     * Construct a rectangle chess board
     *
     * @param horizontalSize number of columns of a board
     * @param verticalSize   number of rows of a board
     */
    public Board(int horizontalSize, int verticalSize) {
        if (horizontalSize < DEFAULT_WIDTH) {
            throw new IllegalArgumentException("horizontalSize should be greater than 7");
        } else {
            this.horizontalSize = horizontalSize;
        }

        if (verticalSize < DEFAULT_HEIGHT) {
            throw new IllegalArgumentException("verticalSize should be greater than 7");
        } else {
            this.verticalSize = verticalSize;
        }
        board = new Piece[horizontalSize][verticalSize];
    }

    /**
     * Given a king in check, return false if a king can get out of check condition, true otherwise
     *
     * @param kingCoordinate the Coordinate of king in check
     * @return true if a king is in checkmate, false otherwise
     */
    public boolean isCheckmate(Coordinate kingCoordinate, History history) {
        Piece king = this.getPiece(kingCoordinate);
        Piece.PieceColor color = king.getColor();

        for (Piece piece : this.getAllPiece()) {
            //Check if any of the piece matching king's color can prevent check
            if (piece.getColor() == color) {
                //For each piece, move to every possible positions
                //if any position prevents check it is not checkmate
                ArrayList<Coordinate> possibleDefense;
                possibleDefense = piece.possibleMoves(this);
                while (!possibleDefense.isEmpty()) {
                    ArrayList<Piece> currentState = saveState();
                    Coordinate newSpot = possibleDefense.remove(0);
                    piece.makeMove(this, history, newSpot);
                    if (this.isInCheck() == null) {
                        this.setPieces(currentState);
                        history.removeHistory();
                        return false;
                    } else {
                        this.setPieces(currentState);
                    }
                }
            }
        }
        return true;
    }

    /**
     * Given a color need to move, return true if it's stalemate, false otherwise
     *
     * @param turnColor the color of the pieces need to move
     * @return true if it's stalemate, false otherwise
     */
    public boolean isStalemate(Piece.PieceColor turnColor, History history) {
        for (Piece piece : this.getAllPiece()) {
            //Check if any of the piece matching turn color can move without getting into check
            if (piece.getColor() == turnColor) {
                //For each piece, move to every possible positions
                //if any position prevents check it is not checkmate
                ArrayList<Coordinate> possibleDefense;
                possibleDefense = piece.possibleMoves(this);
                while (!possibleDefense.isEmpty()) {
                    ArrayList<Piece> currentState = saveState();
                    Coordinate newSpot = possibleDefense.remove(0);
                    if (piece.makeMove(this, history, newSpot)) {
                        this.setPieces(currentState);
                        history.removeHistory();
                        return false;
                    } else {
                        this.setPieces(currentState);
                    }
                }
            }
        }
        return true;
    }

    /**
     * Check if the current board has a king in check
     *
     * @return the coordinate of a king in check
     */
    public Coordinate isInCheck() {
        ArrayList<Piece> pieces = getAllPiece();
        ArrayList<Coordinate> possibleWhiteAttackPosition = new ArrayList<>();
        ArrayList<Coordinate> possibleBlackAttackPosition = new ArrayList<>();
        int blackKingRow = 0;
        int blackKingCol = 0;
        int whiteKingRow = 0;
        int whiteKingCol = 0;

        for (Piece p : pieces) {
            if (p.getColor() == Piece.PieceColor.WHITE) {
                possibleWhiteAttackPosition.addAll(p.possibleMoves(this));
                if (p instanceof King) {
                    whiteKingRow = p.getCoordinate().getRow();
                    whiteKingCol = p.getCoordinate().getColumn();
                }
            } else {
                possibleBlackAttackPosition.addAll(p.possibleMoves(this));
                if (p instanceof King) {
                    blackKingRow = p.getCoordinate().getRow();
                    blackKingCol = p.getCoordinate().getColumn();
                }
            }
        }

        for (Coordinate c : possibleWhiteAttackPosition) {
            if (blackKingRow == c.getRow() && blackKingCol == c.getColumn()) {
                return new Coordinate(c.getRow(), c.getColumn());
            }
        }

        for (Coordinate c : possibleBlackAttackPosition) {
            if (whiteKingRow == c.getRow() && whiteKingCol == c.getColumn()) {
                return new Coordinate(c.getRow(), c.getColumn());
            }
        }
        return null;
    }

    /**
     * Create piece objects and add to the arraylist
     */
    public void setPiecesOnRectangleBoard() {
        pieces.clear();
        //Adding eight pawns for each color
        for (int i = 0; i < DEFAULT_WIDTH; i++) {
            pieces.add(new Pawn(Piece.PieceColor.BLACK, new Coordinate(1, i))); //black pawns to 2nd row from the top
            pieces.add(new Pawn(Piece.PieceColor.WHITE, new Coordinate(verticalSize - 2, i))); //white pawns to 2nd row from the bottom
        }

        int lastRow = verticalSize - 1;
        //Add rooks
        pieces.add(new Rook(Piece.PieceColor.BLACK, new Coordinate(0, 0))); //black rook on top left
        pieces.add(new Rook(Piece.PieceColor.BLACK, new Coordinate(0, DEFAULT_WIDTH - 1))); //black root on top right
        pieces.add(new Rook(Piece.PieceColor.WHITE, new Coordinate(lastRow, 0))); //white rook on bottom left
        pieces.add(new Rook(Piece.PieceColor.WHITE, new Coordinate(lastRow, DEFAULT_WIDTH - 1))); //white rook on bottom right

        //Add knights
        pieces.add(new Knight(Piece.PieceColor.BLACK, new Coordinate(0, 1))); //black knight on top, 2nd left column
        pieces.add(new Knight(Piece.PieceColor.BLACK, new Coordinate(0, DEFAULT_WIDTH - 2))); //black knight on top, 2nd right column
        pieces.add(new Knight(Piece.PieceColor.WHITE, new Coordinate(lastRow, 1))); //white knight on bottom, 2nd left column
        pieces.add(new Knight(Piece.PieceColor.WHITE, new Coordinate(lastRow, DEFAULT_WIDTH - 2))); //white knight on bottom, 2nd right column

        //Add bishops
        pieces.add(new Bishop(Piece.PieceColor.BLACK, new Coordinate(0, DEFAULT_WIDTH - 6))); //black bishop on top, 3rd left column
        pieces.add(new Bishop(Piece.PieceColor.BLACK, new Coordinate(0, DEFAULT_WIDTH - 3))); //black bishop on top, 3rd right column
        pieces.add(new Bishop(Piece.PieceColor.WHITE, new Coordinate(lastRow, DEFAULT_WIDTH - 6))); //white bishop on bottom, 3rd left column
        pieces.add(new Bishop(Piece.PieceColor.WHITE, new Coordinate(lastRow, DEFAULT_WIDTH - 3))); //white bishop on the bottom, 3rd right column

        //Add queens
        pieces.add(new Queen(Piece.PieceColor.BLACK, new Coordinate(0, DEFAULT_WIDTH - 5))); //black queen on top, 4th left column
        pieces.add(new Queen(Piece.PieceColor.WHITE, new Coordinate(lastRow, DEFAULT_WIDTH - 5))); //white queen on bottom, 4th left column

        //add kings
        pieces.add(new King(Piece.PieceColor.BLACK, new Coordinate(0, DEFAULT_WIDTH - 4))); //black king on top, 5th left column
        pieces.add(new King(Piece.PieceColor.WHITE, new Coordinate(lastRow, DEFAULT_WIDTH - 4))); //white king on top, 5th left column
    }

    public void setPieces(ArrayList<Piece> pieces) {
        this.pieces = pieces;
    }

    /**
     * Empty a spot with a given row and column position
     *
     * @param coordinate the coordinate position to be emptied
     */
    public void removePiece(Coordinate coordinate) {
        for (Piece p : pieces) {
            if (p.getCoordinate().getRow() == coordinate.getRow() && p.getCoordinate().getColumn() == coordinate.getColumn()) {
                pieces.remove(p);
                break;
            }
        }
        setPieces(pieces);
    }

    /**
     * Return a piece object with a given row and column position
     *
     * @param coordinate the coordinate position to be searched
     * @return the piece object in a given position if the position is occupied,
     * null if the position is empty
     */
    public Piece getPiece(Coordinate coordinate) {
        for (Piece p : pieces) {
            if (p.getCoordinate().getColumn() == coordinate.getColumn() && p.getCoordinate().getRow() == coordinate.getRow()) {
                return p;
            }
        }
        return null;
    }

    public ArrayList<Piece> getAllPiece() {
        return pieces;
    }

    public int getHorizontalSize() {
        return horizontalSize;
    }

    public int getVerticalSize() {
        return verticalSize;
    }

    /**
     * Check if there are still two kings on the board
     *
     * @return true if there are two kings, false otherwise
     */
    public boolean gameOver() {
        int kingCounter = 0;
        for (Piece p : pieces) {
            if (p instanceof King) {
                kingCounter++;
            }
        }
        return (kingCounter < 2);
    }

    /**
     * Check if a given position is on the board
     *
     * @param coordinate the coordinate position to be checked
     * @return true if a given position is on the board, false otherwise
     */
    public boolean isOnBoard(Coordinate coordinate) {
        int row = coordinate.getRow();
        int column = coordinate.getColumn();
        return !(row < 0 || column < 0) && !(row >= horizontalSize || column >= verticalSize);
    }

    /**
     * Print out to console visually
     */
    public void consolePrint() {

        //Set all the pieces into 2d Array
        for (Piece p : pieces) {
            int row = p.coordinate.getRow();
            int col = p.coordinate.getColumn();
            board[row][col] = p;
        }
        for (int row = 0; row < horizontalSize; row++) {
            for (int col = 0; col < verticalSize; col++) {
                //If a piece exist, print its type
                if (getPiece(new Coordinate(row, col)) != null) {
                    System.out.print("|");
                    String s = getPiece(new Coordinate(row, col)).getClass().toString();
                    s = s.substring(s.lastIndexOf('.') + 1);
                    String typeName = s;
                    String color = getPiece(new Coordinate(row, col)).getColor().toString();
                    if (color.equals("WHITE")) {
                        typeName = "W " + typeName + " " + row + col;
                    } else {
                        typeName = "B " + typeName + " " + row + col;
                    }
                    int stdWordLength = typeName.length();
                    System.out.print(typeName);
                    for (int numSpace = 0; numSpace < 12 - stdWordLength; numSpace++) {
                        System.out.print(" ");
                    }
                    System.out.print("|");
                } else { //If it's a empty space, print empty space
                    System.out.print("|");
                    System.out.print(row + "" + col);
                    for (int length = 0; length < 10; length++) {
                        System.out.print(" ");
                    }
                    System.out.print("|");
                }
            }
            System.out.println();
            //Line breaker between rows
            for (int col = 0; col < verticalSize; col++) {
                System.out.print("--------------");
            }
            System.out.println();
        }
    }


    /**
     * Temporarily save a current state in checking stalemate, checkmate
     *
     * @return the ArrayList of current state of pieces
     */
    private ArrayList<Piece> saveState() {
        ArrayList<Piece> temp = new ArrayList<>();
        for (Piece p : this.getAllPiece()) {
            if (p instanceof Bishop) {
                temp.add(new Bishop(p.getColor(), p.getCoordinate()));
            } else if (p instanceof King) {
                temp.add(new King(p.getColor(), p.getCoordinate()));
            } else if (p instanceof Knight) {
                temp.add(new Knight(p.getColor(), p.getCoordinate()));
            } else if (p instanceof Pawn) {
                temp.add(new Pawn(p.getColor(), p.getCoordinate()));
            } else if (p instanceof Queen) {
                temp.add(new Queen(p.getColor(), p.getCoordinate()));
            } else if (p instanceof Rook) {
                temp.add(new Rook(p.getColor(), p.getCoordinate()));
            }
        }
        return temp;
    }
}
