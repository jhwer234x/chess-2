package general;

/**
 * Coordinate object contains both the row and column index of a chess piece
 */
public final class Coordinate {
    private final int row;
    private final int column;

    /**
     * Default constructor for Coordinate object
     *
     * @param row    the row to set to a Coordinate
     * @param column the column to set to a Coordinate
     */
    public Coordinate(int row, int column) {
        this.row = row;
        this.column = column;
    }

    public int getRow() {
        return row;
    }

    public int getColumn() {
        return column;
    }
}